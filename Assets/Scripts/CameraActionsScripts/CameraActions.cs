﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraActions : MonoBehaviour
{
    public GameObject CameraTarget;
    public float xMin;
    public float xMax;
    public float yMin;
    public float yMax;

    

    // Start is called before the first frame update
    void Start()
    {
        
        
        
    }

    private void Update()
    {
        
    }


    // in this LateUpdate the camera is clamped between two values with an object in the middle.
    void LateUpdate()
    {
        float x = Mathf.Clamp(CameraTarget.transform.position.x, xMin, xMax);
        float y = Mathf.Clamp(CameraTarget.transform.position.y, yMin, yMax);
        gameObject.transform.position = new Vector3(x, y, gameObject.transform.position.z);
    }
}

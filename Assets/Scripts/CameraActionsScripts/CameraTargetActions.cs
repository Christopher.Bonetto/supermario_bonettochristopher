﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTargetActions : MonoBehaviour
{
    public float distance;

    public LayerMask AvoidLayer;

    public GameObject Player;

    PlayerActions teleportCamera;

    // Start is called before the first frame update
    void Start()
    {
        teleportCamera = Player.GetComponent<PlayerActions>();
    }

    // CameraTargetActions check if the player's x position is greater than him. if it happens it translate his x position
    void Update()
    {
        

        if (teleportCamera.teleported)
        {
            Vector2 teleport = new Vector2(2.2f, 10);
            transform.position = teleport;
        }
        if(teleportCamera.teleported == false)
        {
            if(gameObject.transform.position.y > 5)
            {
                Vector2 teleport = new Vector2(18, 2.3f);
                transform.position = teleport;
            }

            if(Player.transform.position.x > this.gameObject.transform.position.x)
            {
                transform.Translate(0.03f, 0, 0);
            }



            //RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down * transform.localScale.x, distance, AvoidLayer);

            //if (hit.transform.tag == "Player")
            //{
            //    transform.Translate(0.02f, 0, 0);
            //}

            
        }




    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawLine(transform.position, (Vector2)transform.position + Vector2.down * transform.localScale.x * distance);
        
    }
}

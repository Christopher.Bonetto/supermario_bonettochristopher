﻿using UnityEngine;
using UnityEngine.UI;


public class EndGameScript : MonoBehaviour
{
    public Text EndGameText;

    // Start is called before the first frame update
    void Start()
    {
        gameObject.SetActive(false);
    }

    // set the variables text with a GameManager's value.
    void Update()
    {
        EndGameText.text = "Ended game with " + " " + GameManager.instance.TotalGameScore.ToString() + " " + " points " + " " + "pls restart the game";
    }
}

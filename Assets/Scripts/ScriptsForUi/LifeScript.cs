﻿
using UnityEngine;
using UnityEngine.UI;


public class LifeScript : MonoBehaviour
{
    public Text lifeText;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // set the variables text with a GameManager's value.
    void Update()
    {
        lifeText.text = "Lives " + "" + GameManager.instance.TotalLives.ToString();
    }
}

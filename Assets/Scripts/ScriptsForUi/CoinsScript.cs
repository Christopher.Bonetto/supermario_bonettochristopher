﻿using UnityEngine;
using UnityEngine.UI;


public class CoinsScript : MonoBehaviour
{
    public Text CoinsText;

    // Start is called before the first frame update
    void Start()
    {

    }

    // set the variables text with a GameManager's value.
    void Update()
    {
        CoinsText.text = "Coins " + "" + GameManager.instance.CoinScore.ToString();
    }
}

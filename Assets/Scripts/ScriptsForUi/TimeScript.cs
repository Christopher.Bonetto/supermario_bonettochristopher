﻿
using UnityEngine;
using UnityEngine.UI;


public class TimeScript : MonoBehaviour
{
    public Text timeText;

    // Start is called before the first frame update
    void Start()
    {

    }

    // set the variables text with a GameManager's value.
    void Update()
    {
        timeText.text = "Time " + "" + GameManager.instance.TotalTimeScore.ToString();
    }
}

﻿
using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour
{
    public Text scoreText;

    // Start is called before the first frame update
    void Start()
    {
        
    }

     // set the variables text with a GameManager's value.
    void Update()
    {
        scoreText.text = "Score "+ "" + GameManager.instance.TotalScore.ToString();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBallActions : MonoBehaviour
{

    Rigidbody2D rb;
    public float force;

    //take the rigidbody component
    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    // At the start destroy this object with a timer
    void Start()
    {
        
        Destroy(this.gameObject, 0.8f);
        
    }

   
    //method for move the fireball (RIGHT)
    public void MoveFireBallRight()
    {
        rb.AddForce(Vector2.right * force);
    }
    //method to move the fireball(LEFT)
    public void MoveFireBallLeft()
    {
        rb.AddForce(Vector2.left * force);
    }

    //check the collisions with others objects
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.tag == "Mushroom" || collision.transform.tag == "Flower" || collision.transform.tag == "Shield" || collision.transform.tag == "MovingShield")
        {
            GameManager.instance.TotalScore += 10;
            Destroy(collision.gameObject);
            Destroy(this.gameObject);
        }
        else if(collision.transform.tag == "Water" || collision.transform.tag == "IndistructibleBlock" || collision.transform.tag == "CoinBlock" || collision.transform.tag == "DistructibleBlock" || collision.transform.tag == "Block")
        {
            Destroy(this.gameObject);
        }
        else if (collision.transform.tag == "Goomba" || collision.transform.tag == "Kroopa")
        {
            Destroy(this.gameObject, 0.05f);
        }
    }
   
}

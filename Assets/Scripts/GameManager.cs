﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public int TotalScore;
    public int TotalLives;

    public int CoinScore;
    
    public int TotalTimeScore;
    private float timeScore;

    public int TotalGameScore;

    public GameObject Player;
    PlayerActions finishGame;

    public GameObject EndedGame;
    EndGameScript TotalGameScoreText;


    private void Awake()
    {
        instance = this;
        
    }
    // Start is called before the first frame update
    void Start()
    {
        TotalLives = PlayerActions.Lives;
        finishGame = Player.GetComponent<PlayerActions>();
        TotalGameScoreText = EndedGame.GetComponent<EndGameScript>();
    }

    // Update is called once per frame
    void Update()
    {
        if(finishGame.endGame == false)
        {
            timeScore += Time.deltaTime;
            if (timeScore >= 1)
            {
                TotalTimeScore--;
                timeScore = 0;
            }


            if (TotalLives == 0 && Input.GetKey("r"))
            {
                PlayerActions.Lives = 3;
            }
            if (CoinScore == 100)
            {
                TotalLives++;
                CoinScore = 0;
                
            }
        }
        else if (finishGame.endGame)
        {
            EndedGame.SetActive(true);
            TotalGameScore = TotalTimeScore + TotalLives * 100 + TotalScore;
        }

        
        
    }
    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructibleBlockActions : MonoBehaviour
{
    

    public int countCoin;

    private bool canInstantiate;

    public GameObject coinPrefab;
    
    public Transform SpawnCoin;

    public bool move;
    public bool canBeHitted;
    public float timerMove;

    private Vector2 initialPos;

    // Start is called before the first frame update
    void Start()
    {
        initialPos = new Vector2(gameObject.transform.position.x, gameObject.transform.position.y);

        canInstantiate = true;

        canBeHitted = true;
    }

    // check if the block must change tag and if it has collided with the player it does a movement.
    void Update()
    {

       if(move && canBeHitted == false)
        {
            timerMove += Time.deltaTime;

            if(timerMove < 0.1f)
            {
                transform.Translate(0, 0.02f, 0);
                
            }
            else if(timerMove >= 0.2f && gameObject.transform.position.y > initialPos.y)
            {   
                transform.Translate(0, -0.02f, 0);
                                
            }
            else if(gameObject.transform.position.y == initialPos.y)
            {
                timerMove = 0;
                canBeHitted = true;
                move = false;
            }
            
        }

       

        if (countCoin == 0)
        {
            canInstantiate = false;
            this.transform.gameObject.tag = "DistructibleBlock";
        }
        
    }

    
    //check if the player collides with the block and if can will spawn one Coin object
    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.tag == "Player" || collision.transform.tag == "StarMode")
        {
            if (canBeHitted)
            {
                move = true;
                canBeHitted = false;

                if (canInstantiate == true && countCoin > 0)
                {
                    GameManager.instance.TotalScore += 100;
                    GameManager.instance.CoinScore += 1;
                    GameObject TemporaryCoin = Instantiate(coinPrefab, SpawnCoin.position, transform.rotation) as GameObject;
                    TemporaryCoin.GetComponent<CoinActions>().canDestroy = true;
                    TemporaryCoin.tag = "Coin";
                    countCoin--;
                }

            }

        }
        
    }
}

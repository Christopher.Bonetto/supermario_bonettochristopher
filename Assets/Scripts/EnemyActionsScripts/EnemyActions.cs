﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyActions : MonoBehaviour
{
    public float EnemySpeed;

    Rigidbody2D rb2D;

    public Transform RayStart;

    protected bool canFlip = false;
    protected bool facingRight = false;

    public bool ActiveEnemy = false;

    public GameObject Shield;

    public bool enemyDeath;
    public int deathAddForceCounter;

    // take rigidbody to the object
    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();

        
    }

    // Update is called once per frame
    protected void Update()
    {
        
        if (ActiveEnemy == true)
        {

            transform.Translate(EnemySpeed, 0, 0);

            CheckPlayer();

            FlipEnemy();

           
        }

       

    }

    //this method check if the enemy find the player above his head
    protected void CheckPlayer()
    {
        RaycastHit2D hit;

        RaycastHit2D hit2;
        Vector2 vectorRayLeft = new Vector2(RayStart.position.x - 0.1f, RayStart.position.y);

        RaycastHit2D hit3;
        Vector2 vectorRayRight = new Vector2(RayStart.position.x + 0.1f, RayStart.position.y);

        if (hit = Physics2D.Raycast(RayStart.transform.position, Vector2.up, 0.1f))
        {

            if (hit.transform.tag == "Player")
            {
                if(gameObject.transform.tag == "Kroopa")
                {
                    
                    Destroy(this.gameObject);
                    
                    GameObject TemporaryShield = Instantiate(Shield, gameObject.transform.position, Quaternion.identity) as GameObject;
                    TemporaryShield.tag = "Shield";
                    TemporaryShield.GetComponent<ShieldActions>();
                    
                }
                else
                {
                    GameManager.instance.TotalScore += 10;


                    EnemyDeathMode();
                }
                
                
            }

            
        }
        


        
        else if (hit2 = Physics2D.Raycast(vectorRayLeft, Vector2.up, 0.1f))
        {
            if (hit2.transform.tag == "Player")
            {
                if (gameObject.transform.tag == "Kroopa")
                {

                    Destroy(this.gameObject);

                    GameObject TemporaryShield = Instantiate(Shield, gameObject.transform.position, Quaternion.identity) as GameObject;
                    TemporaryShield.tag = "Shield";
                    TemporaryShield.GetComponent<ShieldActions>();

                }
                else
                {
                    GameManager.instance.TotalScore += 10;

                    EnemyDeathMode();
                }


            }
            
        }
        
        

       
        else if (hit3 = Physics2D.Raycast(vectorRayRight, Vector2.up, 0.1f))
        {
            if (hit3.transform.tag == "Player")
            {
                if (gameObject.transform.tag == "Kroopa")
                {

                    Destroy(this.gameObject);

                    GameObject TemporaryShield = Instantiate(Shield, gameObject.transform.position, Quaternion.identity) as GameObject;
                    TemporaryShield.tag = "Shield";
                    TemporaryShield.GetComponent<ShieldActions>();

                }
                else
                {
                    GameManager.instance.TotalScore += 10;
                    EnemyDeathMode();
                }


            }
            
        }


        Debug.DrawRay(RayStart.transform.position, Vector2.up * 0.1f, Color.black);
        Debug.DrawRay(vectorRayLeft, Vector2.up * 0.1f, Color.green);
        Debug.DrawRay(vectorRayRight, Vector2.up * 0.1f, Color.blue);
    }

    private void EnemyDeathMode()
    {
        this.gameObject.GetComponent<Collider2D>().enabled = false;
        if (enemyDeath && deathAddForceCounter > 0)
        {
            rb2D.AddForce(Vector2.up * 100f);
            deathAddForceCounter--;
        }
        else if (deathAddForceCounter == 0)
        {
            
        }
    }
    

    

    //this method flip enemy mesh
    protected void FlipEnemy()
    {
        if (canFlip)
        {

            facingRight = !facingRight;
            Vector2 localScale = gameObject.transform.localScale;
            localScale.x *= -1;
            transform.localScale = localScale;
            canFlip = false;
        }
    }


    // check various collide options
    protected void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.tag == "Block" || collision.transform.tag == "Goomba" || collision.transform.tag == "Mushroom" || collision.transform.tag == "Kroopa" || collision.transform.tag == "Water" || collision.transform.tag == "Pipe")
        {
            EnemySpeed *= -1;

            

            canFlip = true;
        }
        else if(collision.transform.tag == "LimitWall")
        {
            Destroy(this.gameObject);
        }
        if(collision.transform.tag == "MovingShield" || collision.transform.tag == "FireBall" || collision.transform.tag == "StarMode")
        {
            GameManager.instance.TotalScore += 10;
            EnemyDeathMode();
        }
       
    }

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.transform.tag == "ActivateEnemiesOnTrigger")
        {
            ActiveEnemy = true;
        }
    }
}

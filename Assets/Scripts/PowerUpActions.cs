﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpActions : MonoBehaviour
{

    public float PowerUpSpeed;
    
    

    
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Check with tag have the object to give him the right movement
    void Update()
    {
        if(gameObject.transform.tag == "Mushroom")
        {
            //Move();
            transform.Translate(PowerUpSpeed, 0, 0);
        }
        else if(gameObject.transform.tag == "Star")
        {
            transform.Translate(PowerUpSpeed,0,0);
        }

        
    }


   

    ////method to move the mushroom
    //void Move()
    //{
    //    RaycastHit2D hit = Physics2D.Raycast(transform.position, new Vector2(xMoveDirection, 0));
    //    gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(xMoveDirection, 0) * PowerUpSpeed;

    //    if (hit.distance < 0.15f)
    //    {
    //        Flip();
    //    }
    //}
    ////method to flip the mushroom
    //void Flip()
    //{
    //    if (xMoveDirection >= 0)
    //    {
    //        xMoveDirection = -1;
    //    }
    //    else
    //    {
    //        xMoveDirection = 1;
    //    }
    //}
    // check the collisions with others objects and when it happens will change some values.
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(gameObject.transform.tag == "Mushroom")
        {
            if(collision.gameObject.tag == "Block" || collision.gameObject.tag == "CoinBlock" || collision.gameObject.tag == "SpecialBlock" || collision.gameObject.tag == "DistructibleBlock" || collision.gameObject.tag == "Pipe")
            {
                PowerUpSpeed *= -1;
            }
            else if (collision.gameObject.tag == "Goomba" || collision.gameObject.tag == "Kroopa")
            {
                Destroy(this.gameObject);
            }
        }
       
        else if(collision.transform.tag == "Water" || collision.transform.tag == "LimitWall")
        {
            Destroy(this.gameObject);
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialBlockActions : MonoBehaviour
{
    public bool star;
    public bool mush;
    public bool flower;

    private bool canInstantiate;

    public GameObject starPrefab;
    public GameObject mushPrefab;
    public GameObject flowerPrefab;

    public Transform SpawnPowerUp;

    // Start is called before the first frame update
    void Start()
    {
        canInstantiate = true; 
    }

    // Update is called once per frame
    void Update()
    {

       

    }
    

    //check if the player collides with this block and which bool is true. Differents bool values to set which updrade must be instantiate.
    private void OnCollisionEnter2D(Collision2D collision)
    {
        

        if (collision.gameObject.tag == "Player" && canInstantiate == true || collision.gameObject.tag == "StarMode" && canInstantiate == true)
        {
            if (mush == true && flower == false && star == false)
            {
                GameObject Mushroom = Instantiate(mushPrefab, SpawnPowerUp.position, transform.rotation) as GameObject;
                Mushroom.tag = "Mushroom";
                canInstantiate = false;
            }
            else if (mush == false && flower == true && star == false)
            {
                GameObject Flower = Instantiate(flowerPrefab, SpawnPowerUp.position, transform.rotation) as GameObject;
                Flower.tag = "Flower";
                canInstantiate = false;
            }
            else if (mush == false && flower == false && star == true)
            {
                GameObject Star = Instantiate(starPrefab, SpawnPowerUp.position, transform.rotation) as GameObject;
                Star.tag = "Star";
                canInstantiate = false;
            }
        }
    }
}

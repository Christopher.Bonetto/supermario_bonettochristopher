﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinActions : MonoBehaviour
{
    public bool canDestroy;


    // At the start destroy this gameobject if the bool is true.
    void Start()
    {
        if (canDestroy)
        {
            Destroy(this.gameObject, 0.3f);

        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    // check the collisions with others objects.
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.transform.tag == "Player" || collision.transform.tag == "StarMode")
        {
            GameManager.instance.TotalScore += 100;
            GameManager.instance.CoinScore += 1;
            Destroy(this.gameObject);
        }
        if(collision.transform.tag == "MovingShield")
        {
            Destroy(this.gameObject);
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum Status
    {
        death,
        small,
        big,
        flower
        
    }

public class PlayerActions : MonoBehaviour
{

    Rigidbody2D rb;


    public float playerSpeed;
    private bool facingRight = false;
    private float moveX;

    

    public float jumpForce;
    private bool isGrounded;
    public Transform feetPosition;
    public float circleRadius;
    public LayerMask whatIsGround;

    public float jumpTime;
    private float jumpTimeCounter;
    private bool isJumping;


    public Status currentStatus;

    public Animator animator;


    public GameObject FireBall;
    public Transform FireBallSpawnPoint;
    private float timer;
    public float delayFireRate;
    public int numberFireBalls;
    private float timeLoadFireBalls;


    private float timeToTakeDamage;
    public float delayTakeDamage;
    private bool canBeHitted;

    private bool invincible;
    public float timerInvincible;


    private SpriteRenderer spriteRenderer;
    private float timeToChangeColor;
    private float timeToSinceChange;

    public static int Lives = 3;

    public float deathTimer;
    public int deathAddForceCounter;
    public bool isDeath;

    public bool teleported;

    public bool endGame;


    





    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        
    }
    // Set the initial status to the player
    void Start()
    {
        
        currentStatus = Status.small;
    }

    // Detect which status have the player each frame.
    void Update()
    {
        if (endGame == false)
        {

            if (GameManager.instance.TotalTimeScore == 0)
            {
                currentStatus = Status.death;
            }



            switch (currentStatus)
            {
                case Status.death:
                    
                    if (isDeath && deathAddForceCounter > 0)
                    {
                        rb.AddForce(Vector2.up * 100f);
                        deathAddForceCounter--;
                    }
                    else if(deathAddForceCounter == 0)
                    {
                        this.gameObject.GetComponent<Collider2D>().enabled = false;
                    }
                   
                    if (Timer(deathTimer))
                    {
                        if (Lives > 0)
                        {
                            Lives--;
                            SceneManager.LoadScene("SampleScene");

                        }
                    }
                    
                    break;

                case Status.small:

                    StarMode();
                    TakeDamage();
                    Jump();

                    animator.SetBool("haveMush", false);
                    break;

                case Status.big:

                    StarMode();
                    TakeDamage();
                    Jump();
                    if (Input.GetKey("s"))
                    {
                        animator.SetBool("crunch", true);
                    }
                    else if (Input.GetKeyUp("s"))
                    {
                        animator.SetBool("crunch", false);
                    }

                    break;

                case Status.flower:

                    StarMode();
                    TakeDamage();
                    Jump();
                    Shoot();

                    break;
                    

            }
            


            

        } 

    }
    private void FixedUpdate()
    {
        if (endGame == false)
        {

            if (currentStatus == Status.small)
            {
                PlayerMove();
            }
            else if (currentStatus == Status.big)
            {
                PlayerMove();
            }
            else if (currentStatus == Status.flower)
            {
                PlayerMove();
            }
        }
    }


    //method to move the player
    void PlayerMove()
    {
        moveX = Input.GetAxis("Horizontal");

        animator.SetFloat("speed", Mathf.Abs(moveX));

        

        if(moveX < 0.0f && facingRight == false)
        {
            FlipPlayer();
        }
        else if(moveX > 0.0f && facingRight == true)
        {
            FlipPlayer();
        }

        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(moveX * playerSpeed, gameObject.GetComponent<Rigidbody2D>().velocity.y);

        if (Input.GetKey("k"))
        {
            playerSpeed = 1.8f;
        }
        else
        {
            playerSpeed = 1.3f;
        }
    }

    void Jump()
    {
        if(currentStatus == Status.small)
        {
            isGrounded = Physics2D.OverlapCircle(new Vector2(feetPosition.transform.position.x,feetPosition.transform.position.y), circleRadius);
            if (isGrounded == true && Input.GetKeyDown(KeyCode.Space))
            {
                isJumping = true;
                jumpTimeCounter = jumpTime;
                rb.velocity = new Vector2(rb.velocity.x, jumpForce);
            }
            if (isJumping == true && Input.GetKey(KeyCode.Space))
            {
                if (jumpTimeCounter > 0)
                {
                    rb.velocity = new Vector2(rb.velocity.x, jumpForce);
                    jumpTimeCounter -= Time.deltaTime;
                }

                else if (jumpTimeCounter < 0)
                {
                    isJumping = false;
                }
            }
            if (Input.GetKeyUp(KeyCode.Space))
            {
                isJumping = false;
            }
        }
        else if (currentStatus != Status.small)
        {
            isGrounded = Physics2D.OverlapCircle(new Vector2(feetPosition.transform.position.x, feetPosition.transform.position.y-0.2f), circleRadius);
            if (isGrounded == true && Input.GetKeyDown(KeyCode.Space))
            {
                isJumping = true;
                jumpTimeCounter = jumpTime;
                rb.velocity = new Vector2(rb.velocity.x, jumpForce);
            }
            if (isJumping == true && Input.GetKey(KeyCode.Space))
            {
                if (jumpTimeCounter > 0)
                {
                    rb.velocity = new Vector2(rb.velocity.x, jumpForce);
                    jumpTimeCounter -= Time.deltaTime;
                }

                else if (jumpTimeCounter < 0)
                {
                    isJumping = false;
                }
            }
            if (Input.GetKeyUp(KeyCode.Space))
            {
                isJumping = false;
            }
        }

        //isGrounded = Physics2D.OverlapCircle(feetPosition.position, circleRadius);
        
        
    }

    
    //void Jump()
    //{
    //    RaycastHit2D hitJumpRight;
    //    RaycastHit2D hitJumpLeft;
    //    if (currentStatus == Status.small)
    //    {
    //        hitJumpRight = Physics2D.Raycast(new Vector2(transform.position.x - 0.05f, transform.position.y - 0.15f), Vector2.down, 0.1f);
    //        hitJumpLeft = Physics2D.Raycast(new Vector2(transform.position.x + 0.05f, transform.position.y - 0.15f), Vector2.down, 0.1f);
         

    //        if (hitJumpRight.point.y <= this.gameObject.transform.position.y)
    //        {
    //            canJump = true;
    //        }
    //        else if (hitJumpRight.point.y >= this.gameObject.transform.position.y)
    //        {
    //            if (Timer(delayJump))
    //            {
    //                canJump = false;
    //            }
                
    //        }

    //        Debug.DrawRay(new Vector2(transform.position.x + 0.05f, transform.position.y - 0.15f), Vector2.down * 0.1f, Color.green);
    //        Debug.DrawRay(new Vector2(transform.position.x - 0.05f, transform.position.y - 0.15f), Vector2.down * 0.1f, Color.green);
    //    }
    //    else if(currentStatus != Status.small)
    //    {
    //        hitJumpRight = Physics2D.Raycast(new Vector2(transform.position.x - 0.08f, transform.position.y - 0.17f), Vector2.down, 0.1f);
    //        hitJumpLeft = Physics2D.Raycast(new Vector2(transform.position.x + 0.08f, transform.position.y - 0.17f), Vector2.down, 0.1f);

            
    //        if (hitJumpRight.point.y <= this.gameObject.transform.position.y)
    //        {

    //            canJump = true;
    //        }
    //        else if (hitJumpRight.point.y >= this.gameObject.transform.position.y)
    //        {
    //            if (Timer(delayJump))
    //            {
    //                canJump = false;
    //            }
    //        }

    //        Debug.DrawRay(new Vector2(transform.position.x + 0.08f, transform.position.y - 0.17f), Vector2.down * 0.1f, Color.green);
    //        Debug.DrawRay(new Vector2(transform.position.x - 0.08f, transform.position.y - 0.17f), Vector2.down * 0.1f, Color.green);
    //    }

        
    //     if (Input.GetKey("space") && canJump || Input.GetKeyDown("space") && canJump)
    //    {
    //        rb.AddForce(Vector2.up * playerJumpPower);
    //        if (playerJumpPower >= 0)
    //        {

    //            playerJumpPower -= playerJumpPower * 10 / 100;

    //        }
    //    }
    //    else if (Input.GetKeyUp("space"))
    //    {
    //        playerJumpPower = 80;
    //    }


        //if (Input.GetKey("space"))
        //{
        //    if (playerJumpPower >= 0)
        //        {

        //              playerJumpPower -= playerJumpPower * 30 / 100;

        //        }
        //        if (canJump)
        //        {

        //            rb.AddForce(Vector2.up * playerJumpPower);
        //            isGrounded = false;
        //            canJump = false;
        //            if (playerJumpPower >= 0)
        //            {

        //                playerJumpPower -= playerJumpPower * 30 / 100;

        //            }
        //        }
        //}
        //else if (Input.GetKeyDown("space"))
        //{
        //    rb.AddForce(Vector2.up * playerJumpPower);
        //}
        //else if (Input.GetKeyUp("space"))
        //{
        //    isGrounded = false;

        //}

        //else if(isGrounded == false && Timer(delayJump))
        //{
        //    playerJumpPower = 85;
        //    isGrounded = true;
        //    canJump = true;
        //}

        

    //}


    


    //method to flip the player mesh 

    
    void FlipPlayer()
    {
        facingRight = !facingRight;
        Vector2 localScale = gameObject.transform.localScale;
        localScale.x *= -1;
        transform.localScale = localScale;
    }


    //method to give a delay to take damage
    private void TakeDamage()
    {
        if (canBeHitted == false)
        {

            if (Timer(delayTakeDamage))
            {
                canBeHitted = true;

            }
            else
            {
                canBeHitted = false;
            }
        }
    }


    //method to give invulnerability to the player with a timer
    private void StarMode()
    {
        if (invincible)
        {
            gameObject.transform.tag = "StarMode";
            if (Timer(timerInvincible))
            {
                invincible = false;
                gameObject.transform.tag = "Player";
            }
            timeToSinceChange += Time.deltaTime;
            if (spriteRenderer != null && timeToSinceChange >= timeToChangeColor)
            {
                Color newColor = new Color(
                    Random.value,
                    Random.value,
                    Random.value
                    );

                spriteRenderer.color = newColor;
                timeToSinceChange = 0f;
            }

        }
        else
        {
            spriteRenderer.color = Color.white;
        }
    }


    // This method allow the player shoot
    void Shoot()
    {
        if(Input.GetKey("e") && Timer(delayFireRate) && numberFireBalls > 0)
        {
            if (facingRight==false)
            {

                GameObject Fireball = Instantiate(FireBall, FireBallSpawnPoint.transform.position, transform.rotation) as GameObject;
                Fireball.GetComponent<FireBallActions>().MoveFireBallRight();
                FireBall.tag = "FireBall";
                numberFireBalls--;
            }
            else if (facingRight)
            {
                GameObject Fireball = Instantiate(FireBall, FireBallSpawnPoint.transform.position, transform.rotation) as GameObject;
                Fireball.GetComponent<FireBallActions>().MoveFireBallLeft();
                FireBall.tag = "FireBall";
                numberFireBalls--;
            }
        }
        else if(!Input.GetKey("e") || numberFireBalls == 0)
        {
            timeLoadFireBalls += Time.deltaTime;
            if(timeLoadFireBalls > 2f)
            {
                numberFireBalls = 2;
                timeLoadFireBalls = 0;
            }
        }
        
    }

   


    //Simple timer
    private bool Timer(float destinationTime)
    {
        timer += Time.deltaTime;
        if (timer >= destinationTime)
        {
            timer = 0;
            return true;
        }
        return false;
    }



    public void Teleport()
    {
        if (gameObject.transform.position.y < 4)
        {
            Vector2 teleport = new Vector2(0, 10);
            transform.position = teleport;
            teleported = true;
        }
        else if (gameObject.transform.position.y > 4.5)
        {
            Vector2 teleport = new Vector2(16.68f, 0.5f);
            transform.position = teleport;
            teleported = false;
        }
    }

    //Check if the player collides with some others objects and will set or change values.
    private void OnCollisionEnter2D(Collision2D collision)
    {
       

        

        if (collision.gameObject.tag == "Water")
        {
            isDeath = true;
            currentStatus = Status.death;
            animator.SetBool("death", true);
        }
        



        if(collision.gameObject.tag == "Mushroom")
        {
            GameManager.instance.TotalScore += 100;
            if(currentStatus == Status.small)
            {
                currentStatus = Status.big;
                animator.SetBool("haveMush", true);
                Destroy(collision.gameObject);
            }
            else if(currentStatus == Status.flower)
            {
                Destroy(collision.gameObject);
            }
            
        }

        else if(collision.transform.tag == "Flower")
        {
            GameManager.instance.TotalScore += 100;
            animator.SetBool("haveMush", false);
            animator.SetBool("haveFlower", true);
            currentStatus = Status.flower;
            Destroy(collision.gameObject);
        }

        else if (collision.transform.tag == "Star")
        {
            GameManager.instance.TotalScore += 100;
            Destroy(collision.gameObject);
            invincible = true;
           

        }



        if (canBeHitted == true && invincible == false)
        {

            if (collision.transform.tag == "Goomba" && currentStatus == Status.small || collision.transform.tag == "Kroopa" && currentStatus == Status.small || collision.transform.tag == "MovingShield" && currentStatus == Status.small)
            {
                isDeath = true;
                currentStatus = Status.death;
                animator.SetBool("death", true);
                canBeHitted = false;
            }


            else if (collision.transform.tag == "Goomba" && currentStatus == Status.big || collision.transform.tag == "Kroopa" && currentStatus == Status.big || collision.transform.tag == "MovingShield" && currentStatus == Status.big)
            {
                currentStatus = Status.small;

                animator.SetBool("haveMush", false);
                canBeHitted = false;
            }

            else if (collision.transform.tag == "Goomba" && currentStatus == Status.flower || collision.transform.tag == "Kroopa" && currentStatus == Status.flower || collision.transform.tag == "MovingShield" && currentStatus == Status.flower)
            {
                currentStatus = Status.big;
                animator.SetBool("haveFlower", false);
                animator.SetBool("haveMush", true);
                canBeHitted = false;
            }
        }


        if (collision.transform.tag == "DistructibleBlock" && currentStatus == Status.big || collision.transform.tag == "DistructibleBlock" && currentStatus == Status.flower)
        {
            GameManager.instance.TotalScore += 10;
            Destroy(collision.gameObject);
        }
                
    }
    
   
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.transform.tag == "WarpGo" && Input.GetKeyDown("s"))
        {

            Teleport();
            
        }
        else if (collision.transform.tag == "WarpReturn" && Input.GetKey("d"))
        {

            Teleport();
            
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.transform.tag == "EndGame")
        {
            
            endGame = true;
        }
    }

}

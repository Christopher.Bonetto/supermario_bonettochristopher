﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldActions : MonoBehaviour
{
    private float ShieldSpeed;

    Rigidbody2D rb2D;

    public Transform TopRaycast;
    public Transform LeftRaycast;
    public Transform RightRaycast;

    private bool goLeft;
    private bool goRight;

    public bool isMoving;

    private float timeStopped;
    public float timeToChangeForm;
    public GameObject kroopaPrefab;

    // at the start take the rigidbody component, set 0 the speed, and the moving boolean start with "false" value.
    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
        isMoving = false;
        ShieldSpeed = 0;
    }

    // 
    void Update()
    {
        CheckCollision();

        if(goRight)
        {
            transform.Translate(ShieldSpeed, 0, 0);
        }
        else if(goLeft)
        {
            transform.Translate(-ShieldSpeed, 0, 0);
        }

        if(ShieldSpeed != 0)
        {
            isMoving = true;
            gameObject.transform.tag = "MovingShield";
        }
        else
        {

            isMoving = false;
            gameObject.transform.tag = "Shield";

        }
        if(isMoving == false)
        {
            timeStopped += Time.deltaTime;
            if(timeStopped >= timeToChangeForm)
            {
                Destroy(this.gameObject);
                GameObject Kroopa = Instantiate(kroopaPrefab, gameObject.transform.position, Quaternion.identity) as GameObject;
                Kroopa.tag = "Kroopa";
                Kroopa.GetComponent<KroopaActions>().ActiveEnemy = true;
                
                timeStopped = 0f;
            }
        }
    }
    // check if the shield collides with the player with differents raycast
    private void CheckCollision()
    {
        RaycastHit2D hitTop;

        RaycastHit2D hitTopLeft;
        Vector2 vectorTopLeft = new Vector2(TopRaycast.position.x - 0.05f, TopRaycast.position.y);

        RaycastHit2D hitTopRight;
        Vector2 vectorTopRight = new Vector2(TopRaycast.position.x + 0.05f, TopRaycast.position.y);

        if (hitTop = Physics2D.Raycast(TopRaycast.transform.position, Vector2.up, 0.1f))
        {

            if (hitTop.transform.tag == "Player")
            {
                ShieldSpeed = 0;
                
            }

        }
        

        
        else if (hitTopLeft = Physics2D.Raycast(vectorTopLeft, Vector2.up, 0.1f))
        {

            if (hitTopLeft.transform.tag == "Player")
            {
                ShieldSpeed = 0;

            }

        }
        

        
        else if (hitTopRight = Physics2D.Raycast(vectorTopRight, Vector2.up, 0.1f))
        {

            if (hitTopRight.transform.tag == "Player")
            {
                ShieldSpeed = 0;

            }

        }
        Debug.DrawRay(TopRaycast.transform.position, Vector2.up * 0.1f, Color.green);
        Debug.DrawRay(vectorTopLeft, Vector2.up * 0.1f, Color.green);
        Debug.DrawRay(vectorTopRight, Vector2.up * 0.1f, Color.green);



        if (isMoving == false)
        {

            RaycastHit2D hitLeft;
            if (hitLeft = Physics2D.Raycast(LeftRaycast.transform.position, Vector2.left, 0.1f))
            {

                if (hitLeft.transform.tag == "Player")
                {
                    ShieldSpeed = 0.03f;
                    goLeft = false;
                    goRight = true;

                }

            }
            Debug.DrawRay(LeftRaycast.transform.position, Vector2.left * 0.1f, Color.blue);

            RaycastHit2D hitRight;
            if (hitRight = Physics2D.Raycast(RightRaycast.transform.position, Vector2.left, 0.1f))
            {

                if (hitRight.transform.tag == "Player")
                {
                    ShieldSpeed = 0.03f;
                    goRight = false;
                    goLeft = true;

                }

            }
            Debug.DrawRay(RightRaycast.transform.position, Vector2.left * 0.1f, Color.blue);
        }
    }
    // check if the shield collides with some objects. if this happens will change some values.
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (gameObject.transform.tag == "MovingShield")
        {

            if (collision.transform.tag == "Block" || collision.transform.tag == "Water" || collision.transform.tag == "Pipe")
            {
                goRight = !goRight;
                goLeft = !goLeft;


            }
            else if (collision.transform.tag == "Mushroom" || collision.transform.tag == "Flower"  || collision.transform.tag == "Coin")
            {
                Destroy(collision.gameObject);

            }
            else if(collision.transform.tag == "LimitWall" || collision.transform.tag == "MovingShield")
            {
                Destroy(this.gameObject);
            }
        }
        else if(gameObject.transform.tag == "Shield")
        {
            if (collision.transform.tag == "LimitWall" || collision.transform.tag == "MovingShield" || collision.transform.tag == "StarMode")
            {
                Destroy(this.gameObject);
            }
            else if (collision.transform.tag == "Mushroom" || collision.transform.tag == "Flower"|| collision.transform.tag == "Coin")
            {
                Destroy(collision.gameObject);

            }
        }
        
    }


}
